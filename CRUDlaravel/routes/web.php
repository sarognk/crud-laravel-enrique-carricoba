<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpleadoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\DepartamentoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Aqui le indicamos a donde va nada mas abrir laravel. en este caso a nuestro login
Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/empleado', function () {
//     return view('empleado.index');
// });

// Route::get('/empleado/create',[EmpleadoController::class,'create']);

//Importante, ese final middleware auth lo que hace es pedir siempre que te loguees
//es decir, si pones la direccion de create y no te has logueado te lleva al login
//un nivel de seguridad para que  no te puedan entrar solo con el enlace
Route::resource('empleado',EmpleadoController::class)->middleware('auth');

//Aqui le estamos diciendo que no muestre en la ventana de autentific. el register
//para que nadie pueda crear otra cuenta y acceder.
Auth::routes(['register'=>false]);

Route::get('/home', [EmpleadoController::class, 'index'])->name('home');

//Cuando el usuario se loguee, se va al index de empleadoController.
Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [EmpleadoController::class, 'index'])->name('index');

});



                    //RUTAS PARA CURSO

 Route::resource('curso',CursoController::class)->middleware('auth');
 Auth::routes(['register'=>false]);  

Route::get('/curso', [CursoController::class, 'index'])->name('curso');


Route::group(['middleware' => 'auth'], function () {

  Route::get('/', [CursoController::class, 'index'])->name('cursoIndex');

});

                   //RUTAS PARA DEPARTAMENTO


 Route::resource('departamento',DepartamentoController::class)->middleware('auth');
 Auth::routes(['register'=>false]);  

Route::get('/departamento', [DepartamentoController::class, 'index'])->name('departamento');


Route::group(['middleware' => 'auth'], function () {

  Route::get('/', [DepartamentoController::class, 'index'])->name('departamentoIndex');

});