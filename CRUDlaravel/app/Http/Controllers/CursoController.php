<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    //Vamos a consultar la informacion cogiendo los 5 primeros registros
    //Lo vamos a almacenar en una variable 
    {
        $datos['cursos']=Curso::paginate(20);
        return view('curso.index',$datos); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


   
    public function create()
     //Le damos al controlador la informacion de la vista.
    // Cuando accedamosa create, se va a la vista.
    {
        return view('curso.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //El Store recibe toda la informacion y la guarda directamente en la BD.
    {
        //$datosEmpleado = request()->all();
        //Le quitamos el token, que es la clave de seguridad del formulario, que no la necesitamos.
        $datosCurso = request()->except('_token');

        //Si hay una foto en el campo del formulario Foto, le decimos que la guarde en uploads
        //Sino lo que hace es subirtela a BD pero sin ser una foto
        if($request->hasFile('Foto')){
            $datosCurso['Foto']=$request->file('Foto')->store('uploads', 'public');
        }
        //Una vez hemos quitado lo que no queremos insertar, ya podemos hacer el insert.
        Curso::insert($datosCurso);
        //Una impresion para confirmar que estan saliendo los datos que queremos
        //return response()->json($datosEmpleado);
        return redirect('curso')->with('mensaje', '*Curso creado correctamente*');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curso  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curso  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Creamos una variable a la que le vamos a pasar una id, y con los datos de esa id rellena
        // nuestro empleado
        $curso=Curso::findOrFail($id);

        return view('curso.edit', compact('curso')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Que en el update se recepcione todo menos el token y el metodo(Patch)
        $datosCurso = request()->except('_token','_method');

        if($request->hasFile('Foto')){
            //Recuperamos la informacion de curso
            $curso=Curso::findOrFail($id);
            
            Storage::delete('public/'.$curso->Foto);
            //Si hubo un cambio de foto, lo actualizamos.
            $datosCurso['Foto']=$request->file('Foto')->store('uploads', 'public');
        }


        //Con el id que le estamos pasando una vez lo encuentres haces el update.
        Curso::where('id','=',$id)->update($datosCurso);
          //retornamos al formulario ya con los datos actualizados
        $curso=Curso::findOrFail($id);
        return view('curso.edit', compact('curso')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Curso  $empleado
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
//Primero le indicamos que informacion necesita para borrar (id)
    $curso=Curso::findOrFail($id);
//Si hubiera alguna foto almacenada la borramos.
    if(Storage::delete('public/'.$curso->Foto)){

    //Despues le pasamos la id para que borre ese empleado.
    
        Curso::destroy($id);

    }

    
     //Una vez borre le tenemos que redirigir.
     return redirect('curso')->with('mensaje','Curso borrado de la base de datos');

    }
}