<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    //Vamos a consultar la informacion cogiendo los 5 primeros registros
    //Lo vamos a almacenar en una variable 
    {
        $datos['departamentos']=Departamento::paginate(5);
        return view('departamento.index',$datos); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


   
    public function create()
     //Le damos al controlador la informacion de la vista.
    // Cuando accedamosa create, se va a la vista.
    {
        return view('departamento.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //El Store recibe toda la informacion y la guarda directamente en la BD.
    {
        //$datosEmpleado = request()->all();
        //Le quitamos el token, que es la clave de seguridad del formulario, que no la necesitamos.
        $datosDepartamento = request()->except('_token');

        //Si hay una foto en el campo del formulario Foto, le decimos que la guarde en uploads
        //Sino lo que hace es subirtela a BD pero sin ser una foto
        if($request->hasFile('Foto')){
            $datosDepartamento['Foto']=$request->file('Foto')->store('uploads', 'public');
        }
        //Una vez hemos quitado lo que no queremos insertar, ya podemos hacer el insert.
        Departamento::insert($datosDepartamento);
        //Una impresion para confirmar que estan saliendo los datos que queremos
        //return response()->json($datosEmpleado);
        return redirect('departamento')->with('mensaje', '*Departamento creado correctamente*');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Departamento  $empleado
     * @return \Illuminate\Http\Departamento
     */
    public function show(Departamento $departamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Departamento  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Creamos una variable a la que le vamos a pasar una id, y con los datos de esa id rellena
        // nuestro empleado
        $departamento=Departamento::findOrFail($id);

        return view('departamento.edit', compact('departamento')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Que en el update se recepcione todo menos el token y el metodo(Patch)
        $datosDepartamento = request()->except('_token','_method');

        if($request->hasFile('Foto')){
            //Recuperamos la informacion de curso
            $departamento=Departamento::findOrFail($id);
            
            Storage::delete('public/'.$departamento->Foto);
            //Si hubo un cambio de foto, lo actualizamos.
            $datosDepartamento['Foto']=$request->file('Foto')->store('uploads', 'public');
        }


        //Con el id que le estamos pasando una vez lo encuentres haces el update.
        Departamento::where('id','=',$id)->update($datosDepartamento);
          //retornamos al formulario ya con los datos actualizados
        $departamento=Departamento::findOrFail($id);
        return view('departamento.edit', compact('departamento')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Departamento  $empleado
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
//Primero le indicamos que informacion necesita para borrar (id)
    $departamento=Departamento::findOrFail($id);
//Si hubiera alguna foto almacenada la borramos.
    if(Storage::delete('public/'.$departamento->Foto)){

    //Despues le pasamos la id para que borre ese empleado.
    
    Departamento::destroy($id);

    }

    
     //Una vez borre le tenemos que redirigir.
     return redirect('departamento')->with('mensaje','Departamento borrado de la base de datos');

    }
}