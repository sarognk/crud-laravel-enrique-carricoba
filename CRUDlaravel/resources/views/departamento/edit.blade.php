Formulario de edicion de departamentos.
@extends('layouts.app')
@section('content')
<div class="container">

<form action="{{ url('/departamento/'.$departamento->id ) }}" method="POST" enctype="multipart/form-data">
@csrf

{{method_field('PATCH')}}

@include('departamento.form',['modo'=>'Editar']);

</form>
</div>
@endsection