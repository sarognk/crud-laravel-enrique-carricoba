

<div class="p-3 mb-2 bg-dark text-white">
<h2>{{ $modo }} Departamento </h2>

<div class="form-group"> 

    <label for="Nombre"> Nombre </label>
    {{-- Usamos el isset para que si hay algo lo enseñe pero que si no hay nada no falle, simplemente salga vacio. --}}
    <input type="text" class="form-control" name="Nombre" value="{{ isset($departamento->Nombre)?$departamento->Nombre:'' }}" id="Nombre" > 
    </div>
    
    <div class="form-group"> 
    
    <label for="Nombre"> Planta</label>
    <input type="text" class="form-control" name="Planta" value="{{ isset($departamento->Planta) ?$departamento->Planta:'' }}" id="PrimerApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre">Contacto</label>
    <input type="text" class="form-control" name="Contacto" value="{{ isset($departamento->Contacto) ?$departamento->Contacto:'' }}" id="SegundoApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre">Responsable</label>
    <input type="text" class="form-control" name="Responsable" value="{{ isset($departamento->Responsable)?$departamento->Responsable:'' }}" id="Email">
    </div>
    <div class="form-group"> 
    <label for="Foto"></label>
    {{-- Este if es para decirle que si no hay foto no rompa, que no saque nada y siga --}}
    @if(@isset($departamento->Foto))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$departamento->Foto }}" width="100" alt=""> 
    
    @endisset
   
    <img  class="img-thumbnail img-fluid" width="100" alt="">
    <input type="file" name="Foto" value="" id="Foto">
    </div>

    <br>
</div>
    <input type="submit" value="{{ $modo }} datos" > 
    <a class="btn btn-dark" href="{{ url('departamento/')}}">Atras</a>
