@extends('layouts.app')
@section('content')
<div class="container">

<form action="{{ url('/empleado') }}" method="POST" enctype="multipart/form-data">
    @csrf {{--  Esto es una clave de seguridad para que se sepa que el formulario viene del mismo equipo. --}}
    {{-- Le metemos el include de la carpeta empleado y el archivo formulario --}}
    @include('empleado.form',['modo'=>'Insertar'])
</form>
<br>
</div>
@endsection