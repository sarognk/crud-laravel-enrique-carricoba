@extends('layouts.app')
@section('content')
<div class="container">

{{-- Si hay un mensaje, muestralo --}}
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}

@endif
                            
<center><h1><b>PLATAFORMA DE GESTION DE EMPLEADOS</b></h1></center>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href={{asset("empleado")}}>Empleados</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{asset("curso")}}"">Cursos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href={{asset("departamento")}}>Departamentos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
  </nav>

<table class="table table-dark table-striped table-bordered" id="example">
  
        <thead>
            <tr>
                <th>Nº</th>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Correo</th>
                <th>Acciones</th>
               
            </tr>
        </thead>
       
        <tbody>
            {{-- Todo el conjunto de datos que nos llega del controlador 
                al fina se pasa en una variable ($empleado) --}}
        @foreach( $empleados as $empleado )
            <tr>
                <td>{{ $empleado->id }}</td>
    
                <td>
                <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$empleado->Foto }}" width="100" alt="">
                </td>
    
                <td>{{ $empleado->Nombre }}</td>
                <td>{{ $empleado->PrimerApellido }}</td>
                <td>{{ $empleado->SegundoApellido }}</td>
                <td>{{ $empleado->Email }}</td>
                <td>
                {{-- Le mandamos una informacion a empleado, cogemos la id y abrimos edit.blade.php --}}
                <a href="{{ url('/empleado/'.$empleado->id.'/edit') }}" class="btn btn-light">
                
                      Editar 
                      
                </a> 
                



                {{-- Que envie a traves de este formulario el id del usuario que queremos borrar --}}
                <form  action="{{ url('/empleado/'.$empleado->id ) }}" method="post">
                @csrf 
                {{ method_field('DELETE') }}
                <input class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar este empleado?')" 
                value="Borrar">
                    
                </form>
                
                
                </td>
            </tr>
        @endforeach
    
        </tbody>

    </table>

    <a href="{{url('empleado/create') }}" class="btn btn-dark" >Registrar un nuevo empleado </a>
    <br>
    </div>
   
    @endsection
 