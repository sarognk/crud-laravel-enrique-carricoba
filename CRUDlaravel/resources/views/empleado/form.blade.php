

<div class="p-3 mb-2 bg-dark text-white">
<h2>{{ $modo }} empleado </h2>

<div class="form-group"> 

    <label for="Nombre"> Nombre </label>
    {{-- Usamos el isset para que si hay algo lo enseñe pero que si no hay nada no falle, simplemente salga vacio. --}}
    <input type="text" class="form-control" name="Nombre" value="{{ isset($empleado->Nombre)?$empleado->Nombre:'' }}" id="Nombre" > 
    </div>
    
    <div class="form-group"> 
    
    <label for="Nombre"> Primer Apellido</label>
    <input type="text" class="form-control" name="PrimerApellido" value="{{ isset($empleado->PrimerApellido) ?$empleado->PrimerApellido:'' }}" id="PrimerApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre"> Segundo apellido</label>
    <input type="text" class="form-control" name="SegundoApellido" value="{{ isset($empleado->SegundoApellido) ?$empleado->SegundoApellido:'' }}" id="SegundoApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre"> Correo </label>
    <input type="text" class="form-control" name="Email" value="{{ isset($empleado->Email)?$empleado->Email:'' }}" id="Email">
    </div>
    <div class="form-group"> 
    <label for="Foto">Empleado</label>
    {{-- Este if es para decirle que si no hay foto no rompa, que no saque nada y siga --}}
    @if(@isset($empleado->Foto))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$empleado->Foto }}" width="100" alt=""> 
    
    @endisset
   
    <img  class="img-thumbnail img-fluid" width="100" alt="">
    <input type="file" name="Foto" value="" id="Foto">
    </div>

    <br>
</div>
    <input type="submit" value="{{ $modo }} datos" > 
    <a class="btn btn-dark" href="{{ url('empleado/')}}">Atras</a>
