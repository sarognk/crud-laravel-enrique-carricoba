Formulario de edicion de nuestro curso.
@extends('layouts.app')
@section('content')
<div class="container">

<form action="{{ url('/curso/'.$curso->id ) }}" method="POST" enctype="multipart/form-data">
@csrf

{{method_field('PATCH')}}

@include('curso.form',['modo'=>'Editar']);

</form>
</div>
@endsection