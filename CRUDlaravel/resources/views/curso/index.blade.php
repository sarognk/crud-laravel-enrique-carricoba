@extends('layouts.app')
@section('content')
<div class="container">

{{-- Si hay un mensaje, muestralo --}}
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}

@endif
<center><h1><b>PLATAFORMA DE GESTION DE CURSOS</b></h1></center>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href={{asset("empleado")}}>Empleados</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href={{asset("curso")}}>Cursos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href={{asset("departamento")}}>Departamentos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
  </nav>

<table class="table table-dark table-striped table-bordered" id="example">
  
        <thead class="thead-light">
            <tr>
                <th>Nº</th>
                <th>Foto</th>
                <th>Titulo</th>
                <th>Ponente</th>
                <th>Numero de horas</th>
                <th>Informacion</th>
                <th>Acciones</th>
               
            </tr>
        </thead>
       
        <tbody>
            {{-- Todo el conjunto de datos que nos llega del controlador 
                al fina se pasa en una variable ($empleado) --}}
        @foreach( $cursos as $curso )
            <tr>
                <td>{{ $curso->id }}</td>
    
                <td>
                <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$curso->Foto }}" width="100" alt="">
                </td>
    
                <td>{{ $curso->Titulo }}</td>
                <td>{{ $curso->Ponente }}</td>
                <td>{{ $curso->HorasTotal }}</td>
                <td>{{ $curso->Email }}</td>
                <td>
                {{-- Le mandamos una informacion a curso, cogemos la id y abrimos edit.blade.php --}}
                <a href="{{ url('/curso/'.$curso->id.'/edit') }}" class="btn btn-light">
                
                      Editar 
                      
                </a> 
                



                {{-- Que envie a traves de este formulario el id del usuario que queremos borrar --}}
                <form  action="{{ url('/curso/'.$curso->id ) }}" method="post">
                @csrf 
                {{ method_field('DELETE') }}
                <input class="btn btn-danger" type="submit" onclick="return confirm('¿Deseas borrar este curso?')" 
                value="Borrar">
                    
                </form>
                
                
                </td>
            </tr>
        @endforeach
    
        </tbody>
    
    </table>

    <a href="{{url('curso/create') }}" class="btn btn-dark" >Registrar un nuevo curso </a>
    <br>
    </div>
    @endsection