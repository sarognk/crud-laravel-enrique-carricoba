

<div class="p-3 mb-2 bg-dark text-white">
<h2>{{ $modo }} Curso </h2>

<div class="form-group"> 

    <label for="Nombre"> Titulo </label>
    {{-- Usamos el isset para que si hay algo lo enseñe pero que si no hay nada no falle, simplemente salga vacio. --}}
    <input type="text" class="form-control" name="Titulo" value="{{ isset($curso->Titulo)?$curso->Titulo:'' }}" id="Nombre" > 
    </div>
    
    <div class="form-group"> 
    
    <label for="Nombre"> Ponente</label>
    <input type="text" class="form-control" name="Ponente" value="{{ isset($curso->Ponente) ?$curso->Ponente:'' }}" id="PrimerApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre"> Numero de horas</label>
    <input type="text" class="form-control" name="HorasTotal" value="{{ isset($curso->HorasTotal) ?$curso->HorasTotal:'' }}" id="SegundoApellido">
    </div>
    <div class="form-group"> 
    <label for="Nombre"> informacion </label>
    <input type="text" class="form-control" name="Email" value="{{ isset($curso->Email)?$curso->Email:'' }}" id="Email">
    </div>
    <div class="form-group"> 
    <label for="Foto"></label>
    {{-- Este if es para decirle que si no hay foto no rompa, que no saque nada y siga --}}
    @if(@isset($curso->Foto))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$curso->Foto }}" width="100" alt=""> 
    
    @endisset
   
    <img  class="img-thumbnail img-fluid" width="100" alt="">
    <input type="file" name="Foto" value="" id="Foto">
    </div>

    <br>
</div>
    <input type="submit" value="{{ $modo }} datos" > 
    <a class="btn btn-dark" href="{{ url('curso/')}}">Atras</a>
